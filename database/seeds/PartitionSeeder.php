<?php

use Illuminate\Database\Seeder;
use App\Partition;

class PartitionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $partition = Partition::firstOrNew([
            'name' => 'Zone 1',
            'park_id' => 1,
            'user_id' => 13,
            'days_council_service_end' => 25,
        ]);
        $partition->save();
    }
}
