<?php

use Illuminate\Database\Seeder;
use App\Organisation;

class OrganisationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $bc = Organisation::FirstOrNew([
            'name' => 'Belfast City',
        ]);
        $bc->save();

        $an = Organisation::FirstOrNew([
            'name' => 'Antrim Newtownabbey',
        ]);
        $an->save();

        $and = Organisation::FirstOrNew([
            'name' => 'Ards North Down',
        ]);
        $and->save();

        $abc = Organisation::FirstOrNew([
            'name' => 'Armagh Banbridge Craigavon',
        ]);
        $abc->save();


        $ccg = Organisation::FirstOrNew([
            'name' => 'Causeway Coast Glens',
        ]);
        $ccg->save();

        $ds = Organisation::FirstOrNew([
            'name' => 'Derry Strabane',
        ]);
        $ds->save();

        $fo = Organisation::FirstOrNew([
            'name' => 'Fermanagh Omagh',
        ]);
        $fo->save();

        $lc = Organisation::FirstOrNew([
            'name' => 'Lisburn Castlereagh',
        ]);
        $lc->save();

        $mea = Organisation::FirstOrNew([
            'name' => 'Mid East Antrim',
        ]);
        $mea->save();

        $mu = Organisation::FirstOrNew([
            'name' => 'Mid Ulster',
        ]);
        $mu->save();
        
        $nmd = Organisation::FirstOrNew([
            'name' => 'Newry Mourne Down',
        ]);
        $nmd->save();
    }
}
