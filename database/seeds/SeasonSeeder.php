<?php

use Illuminate\Database\Seeder;
use App\Season;

class SeasonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $spring = Season::firstOrNew([
            'season'=>'spring',
        ]);
        $spring->save();

        $summer = Season::firstOrNew([
            'season'=>'summer',
        ]);
        $summer->save();

        $fall = Season::firstOrNew([
            'season'=>'fall',
        ]);
        $fall->save();

        $winter = Season::firstOrNew([
            'season'=>'winter',
        ]);
        $winter->save();
    }

}
