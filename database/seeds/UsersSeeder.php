<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Set Roles
        $adminRole = Role::create(['name' => 'Admin']);
        $adminPermission = Permission::create(['name' => 'all']);
        $citizen = Role::create(['name'=> 'Citizen']);
        $citizenPermission = Permission::create(['name' => 'low']);
        $council = Role::create(['name'=> 'Council']);
        $councilPermsssion = Permission::create(['name'=> 'middle']);
        $adminRole->save();
        $adminPermission->save();
        $citizen->save();
        $citizenPermission->save();
        $council->save();
        $councilPermsssion->save();



        //Set Admin
        $admin = User::FirstOrNew([
            'name' => 'Admin User',
            'email' => 'admin@example.com',
            'password' => Hash::make('password'),
            'address' => 'Admin User',
            'contact_num' => 'Admin User'
        ]);
         //Role Assign
        $getAdminRole = Role::findByID(1);
        echo 'AdminRole Check : ' . $getAdminRole;
        $admin->givePermissionTo($adminPermission);
        $admin->assignRole($getAdminRole);
        $admin->save(); 
        echo 'Admin Check : ' . $admin;



        //Set Organisations
        $bc = User::FirstOrNew([
            'name' => 'Belfast City',
            'email' => 'bc@example.com',
            'password' => Hash::make('password'),
            'address' => '1 City Hall',
            'contact_num' => '07125469132',
            'organisation_id' => 1,
        ]);
        //Role Assign
        $getCouncilRole = Role::findByID(3);
        echo 'Council Role Check : ' . $getCouncilRole;
        $bc->givePermissionTo($councilPermsssion);
        $bc->assignRole($getCouncilRole);
        echo 'Council Check : ' . $bc;
        $bc->save(); 
        


        $an = User::FirstOrNew([
            'name' => 'Antrim Newtownabbey',
            'email' => 'an@example.com',
            'password' => Hash::make('password'),
            'address' => '24 Antrim Rd',
            'contact_num' => '07125469132',
            'organisation_id' => 2,
        ]);
        //Role Assign
        $getCouncilRole = Role::findByID(3);
        echo 'Council Role Check : ' . $getCouncilRole;
        $an->givePermissionTo($councilPermsssion);
        $an->assignRole($getCouncilRole);
        echo 'Council Check : ' . $an;
        $an->save();




        $and = User::FirstOrNew([
            'name' => 'Ards North Down',
            'email' => 'and@example.com',
            'password' => Hash::make('password'),
            'address' => '12 Down Street ',
            'contact_num' => '07125469132',
            'organisation_id' => 3,
        ]);
        //Role Assign
        $getCouncilRole = Role::findByID(3);
        echo 'Council Role Check : ' . $getCouncilRole;
        $and->givePermissionTo($councilPermsssion);
        $and->assignRole($getCouncilRole);
        echo 'Council Check : ' . $and;
        $and->save();



        $abc = User::FirstOrNew([
            'name' => 'Armagh Banbridge Craigavon',
            'email' => 'abc@example.com',
            'password' => Hash::make('password'),
            'address' => '22 Town Rd',
            'contact_num' => '07125469132',
            'organisation_id' => 4,
        ]);
        //Role Assign
        $getCouncilRole = Role::findByID(3);
        echo 'Council Role Check : ' . $getCouncilRole;
        $abc->givePermissionTo($councilPermsssion);
        $abc->assignRole($getCouncilRole);
        echo 'Council Check : ' . $abc;
        $abc->save();



        $ccg = User::FirstOrNew([
            'name' => 'Causeway Coast Glens',
            'email' => 'ccg@example.com',
            'password' => Hash::make('password'),
            'address' => '65 Coast Rd',
            'contact_num' => '07125469132',
            'organisation_id' => 5,
        ]);
        //Role Assign
        $getCouncilRole = Role::findByID(3);
        echo 'Council Role Check : ' . $getCouncilRole;
        $ccg->givePermissionTo($councilPermsssion);
        $ccg->assignRole($getCouncilRole);
        echo 'Council Check : ' . $ccg;
        $ccg->save();




        $ds = User::FirstOrNew([
            'name' => 'Derry Strabane',
            'email' => 'ds@example.com',
            'password' => Hash::make('password'),
            'address' => '99 City Hall',
            'contact_num' => '07125469132',
            'organisation_id' => 6,
        ]);
        //Role Assign
        $getCouncilRole = Role::findByID(3);
        echo 'Council Role Check : ' . $getCouncilRole;
        $ds->givePermissionTo($councilPermsssion);
        $ds->assignRole($getCouncilRole);
        echo 'Council Check : ' . $ds;
        $ds->save();




        $fo = User::FirstOrNew([
            'name' => 'Fermanagh Omagh',
            'email' => 'fo@example.com',
            'password' => Hash::make('password'),
            'address' => '44 Omagh Town Rd',
            'contact_num' => '07125469132',
            'organisation_id' => 7
        ]);
        //Role Assign
        $getCouncilRole = Role::findByID(3);
        echo 'Council Role Check : ' . $getCouncilRole;
        $fo->givePermissionTo($councilPermsssion);
        $fo->assignRole($getCouncilRole);
        echo 'Council Check : ' . $fo;
        $fo->save();




        $lc = User::FirstOrNew([
            'name' => 'Lisburn Castlereagh',
            'email' => 'lc@example.com',
            'password' => Hash::make('password'),
            'address' => '31 Lisburn Rd',
            'contact_num' => '07125469132',
            'organisation_id' => 8,
        ]);
        //Role Assign
        $getCouncilRole = Role::findByID(3);
        echo 'Council Role Check : ' . $getCouncilRole;
        $lc->givePermissionTo($councilPermsssion);
        $lc->assignRole($getCouncilRole);
        echo 'Council Check : ' . $lc;
        $lc->save();




        $mea = User::FirstOrNew([
            'name' => 'Mid East Antrim',
            'email' => 'mea@example.com',
            'password' => Hash::make('password'),
            'address' => 'Antrim Town Hall',
            'contact_num' => '07125469132',
            'organisation_id' => 9
        ]);
        //Role Assign
        $getCouncilRole = Role::findByID(3);
        echo 'Council Role Check : ' . $getCouncilRole;
        $mea->givePermissionTo($councilPermsssion);
        $mea->assignRole($getCouncilRole);
        echo 'Council Check : ' . $mea;
        $mea->save();



        $mu = User::FirstOrNew([
            'name' => 'Mid Ulster',
            'email' => 'mu@example.com',
            'password' => Hash::make('password'),
            'address' => 'Mid Ulster Rd',
            'contact_num' => '07125469132',
            'organisation_id' => 10
        ]);
        //Role Assign
        $getCouncilRole = Role::findByID(3);
        echo 'Council Role Check : ' . $getCouncilRole;
        $mu->givePermissionTo($councilPermsssion);
        $mu->assignRole($getCouncilRole);
        echo 'Council Check : ' . $mu;
        $mu->save();
        


        $nmd = User::FirstOrNew([
            'name' => 'Newry Mourne Down',
            'email' => 'nmd@example.com',
            'password' => Hash::make('password'),
            'address' => 'Mourn Mountain Pass',
            'contact_num' => '07125469132',
            'organisation_id' => 11
        ]);
        //Role Assign
        $getCouncilRole = Role::findByID(3);
        echo 'Council Role Check : ' . $getCouncilRole;
        $nmd->givePermissionTo($councilPermsssion);
        $nmd->assignRole($getCouncilRole);
        echo 'Council Check : ' . $nmd;
        $nmd->save();



        //Set Citizen
        $citizen = User::FirstOrNew([
            'name' => 'Citizen User',
            'email' => 'citizen@example.com',
            'password' => Hash::make('password'),
            'address' => '14 Greater Belfast Area',
            'contact_num' => '07785412369',
            'organisation_id' => 1,
        ]);
        //Role Assign
        $getCitizenRole = Role::findByID(2);
        echo 'Citizen Role Check : ' . $getCitizenRole;
        $citizen->givePermissionTo($citizenPermission);
        $citizen->assignRole($getCitizenRole);
        echo 'Citizen Check : ' . $citizen;
        $citizen->save();

    }
}
