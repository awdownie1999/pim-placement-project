<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call([
            OrganisationSeeder::class,
            ParkSeeder::class,
            UsersSeeder::class,
            PartitionSeeder::class,
            SeasonSeeder::class,
            RecordSeeder::class,
         ]);
    }
}
