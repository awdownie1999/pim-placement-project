<?php

use Illuminate\Database\Seeder;
use App\Park;

class ParkSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $bcPark = Park::firstOrNew([
           'name' => 'Belfast City Park',
           'organisation_id' => 1,
        ]);
        $bcPark->save();
    }
}
