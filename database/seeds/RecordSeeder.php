<?php

use Illuminate\Database\Seeder;
use App\Record;

class RecordSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $record = Record::firstOrNew([
            'report_year' => 2018,
            'season_id' => 1,
            'description' => 'All Facilities have been left as inetended. No further action required',
            'partition_id' => 1,
        ]);
        $record->save();
    }
}
