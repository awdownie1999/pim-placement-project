<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::group(['middleware' => ['auth']], function () {
    Route::resource('parks', 'ParkController');
    Route::resource('account', 'AccountController');
    Route::resource('organisations', 'OrganisationController');
    Route::resource('partitions', 'PartitionController');
    Route::resource('records', 'RecordController');
    Route::resource('users', 'UserController');
    Route::resource('dash', 'DashController');
    Route::post('/partitions/{partition}/addImages/{}', 'PartitionController@addImages')->name('partitions.addImages');
});
Auth::routes(['verify' => true]);


Route::get('/here', function () {
    return view('welcome');
});

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/search', 'ParkController@index')->name('search');
