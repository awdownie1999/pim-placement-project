@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Partition Details</h1>
        <div>
        @if ($errors->any())
            <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
            </div><br/>
            @endif
            <br/>
            <a class="btn btn-primary" href="{{route('records.create', ['partitionID'=>$partition->id])}}">Create Record</a>
            <br/><br/>
            <table>
                <thead>
                    <tr>
                        <th>Partition</th>
                        <th>Park</th>
                        <th>Organisation</th>
                        <th>User</th>
                        <th>Days Left&nbsp;&nbsp;&nbsp;&nbsp;</th>
                        <th>Images</th>
                    </tr>
                </thead>

                <tbody>
                    <tr>
                        <td>&nbsp;&nbsp;{{$partition->name }}</td> 
                        <td>&nbsp;&nbsp;{{$partition->park->name }}</td>
                        <td>&nbsp;&nbsp;{{$partition->park->organisation->name }}</td>
                        @if($partition->user_id != null)
                            <td>&nbsp;&nbsp;{{$partition->user->name }}</td>
                            <td>&nbsp;&nbsp;{{$partition->days_council_service_end}}</td>
                        @else
                            <td>&nbsp;&nbsp;No User is looking after this Zone</td>
                            <td>&nbsp;&nbsp;N/A</td>
                        @endif    
                    </tr>
                </tbody>
            </table>
            @if($partition->getMedia('partition_images') != null)
                @foreach ($partition->getMedia('partition_images') as $image)
                    <td> &nbsp;&nbsp;&nbsp;<a href="{{ $image->getUrl()}}"> <img src="{{ $image->getUrl('small') }}"> </a>  </td>
                @endforeach
            @else
                    <h4>No Images</h4>
            @endif
            <br/>
            <br/>
            <h3>Add Images</h3>
            <form method="post" enctype="multipart/form-data" action="{{ route('partitions.update', $partition->id)}}">
                @csrf
                <br/><input type="file" name="file1" /><br/>
                <input type="file" name="file2" /><br/>
                <input type="file" name="file3" /><br/>
                <button type="submit" class="btn btn-secondary">Add Images</button>
            </form>
        @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    </div>
</div>
</div>
@endsection