@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Partition</h1>
        <div>
        @if ($errors->any())
            <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
            </div><br/>
        @endif
        @hasanyrole('Admin|Council')
            <a style="margin: 19px;" href="{{ route('partitions.index', ['filter'=> 'all']) }}" class="btn btn-primary">All</a>
            <a style="margin: 19px;" href="{{ route('partitions.index', ['filter'=> 'due']) }}" class="btn btn-primary">Due Soon</a>
            <a style="margin: 19px;" href="{{ route('partitions.index', ['filter'=> 'unassigned']) }}" class="btn btn-primary">Unassigned</a>
        @endhasanyrole
        @if($partitions == null)
            <h1>No Partitions matching criteria</h1>
        @else
            <table>
                <thead>
                    <tr>
                        <th>Partition</th>
                        <th>Park</th>
                        <th>Organisation</th>
                        <th>User</th>
                        <th>Days Left</th>
                        
                    </tr>
                </thead>
                <tbody>
                    @foreach ($partitions as $partition )
                        <tr>
                            <td>{{$partition->name }}</td> 
                            <td>{{$partition->park->name }}</td>
                            <td>{{$partition->park->organisation->name }}</td>
                            @if($partition->user_id != null)
                                <td>{{$partition->user->name }}</td>
                                <td>{{$partition->days_council_service_end}}</td>
                            @else
                                <td>No User is looking after this Zone</td>
                                <td>N/A</td>
                            @endif
                            
                            @hasanyrole('Admin|Council')
                            <div>
                                <td><a href="{{route('partitions.edit', $partition->id)}}" class="btn btn-primary">Assign</a></td>
                                <td><a href="{{route('partitions.show', $partition->id)}}" class="btn btn-primary">Show</a></td>
                                <td><form action="{{route('partitions.destroy', $partition->id)}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-primary" type="sumbit"> Delete</button></form></td>
                            @endhasanyrole
                            </div>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @endif
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        </div>
    </div>
</div>
@endsection