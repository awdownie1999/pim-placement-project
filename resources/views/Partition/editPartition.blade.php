@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Assign User</h1>
        <div>
        @if ($errors->any())
            <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
            </div><br/>
        @endif
        @hasanyrole('Admin|Council')
            <form method="post" enctype="multipart/form-data" action="{{ route('partitions.update', $partition->id)}}">
                @method('PATCH')
                @csrf
                <div class="form-group">
                    <label for="user">User:</label>
                    @if($users != null)
                        <select name="user_id">
                            @foreach ($users as $user)
                                <option value="{{$user->id}}">{{ $user->name }}</option>
                            @endforeach
                        </select>
                    @else
                        <h3>No Available Users</h3>
                    @endif
                </div>

                <div class="form-group">
                    <label for="days">Days Left:</label>
                    <input type="number" class="form-control" name="days" value="{{ $partition->days_council_service_end }}">
                </div>
                <button type="submit" class="btn btn-secondary">Update</button>
            </form>
        @endhasanyrole
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        </div>
    </div>
</div>
@endsection