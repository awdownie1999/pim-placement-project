@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Create Partition</h1>
        <div>
        @if ($errors->any())
            <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
            </div><br/>
        @endif
        @hasanyrole('Admin|Council')
            <form method="post" enctype="multipart/form-data" action="{{ route('partitions.store') }}">
                @csrf
                <div class="form-group">
                    <label for="name">name:</label>
                    <input type="text" class="form-control" name="name" />
                </div>
                <div>
                    <label for="park">park</label>
                    <select name="park_id">
                        @foreach ($parks as $park )
                            <option value="{{$park->id}}">{{ $park->name}}</option>
                        @endforeach
                    </select>
                </div>
                <br/><input type="file" name="file1" /><br/><br/>
                <input type="file" name="file2" /><br/><br/>
                <input type="file" name="file3" /><br/><br/>
                <button type="submit" class="btn btn-secondary">Add Zone</button>
            </form>
        @endhasanyrole
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        </div>
    </div>
</div>
@endsection