@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Account</h1>
        <div>
        @if ($errors->any())
            <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
            </div><br/>
        @endif
            <table>
                <tr>
                    <td style="text-align: left;">
                        <ul style="list-style-type: none;">
                            <li>Name: {{Auth::user()->name}}</li>
                            <li>Email: {{Auth::user()->email}}</li>
                            <li>Address: {{Auth::user()->address}}</li>
                            <li>Contact Number: {{Auth::user()->contact_num}}</li>
                            @if(Auth::user()->organisation_id != null)
                            <li>Organisation: {{Auth::user()->organisation->name}}</li>
                            @endif
                            <br />
                        </ul>
                    </tr>
            </table>
            
            <a style="margin: 19px;" class="btn btn-primary" href="{{ route('account.edit', $user->id)}}">Edit Details</a>
            @if(Auth::user()->getMedia('avatars')->first() != null)
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Profile Picture &nbsp;&nbsp;&nbsp;<img src="{{ Auth::user()->getFirstMediaUrl('avatars', 'square') }}">
            <br />
            <br />
            <form action="{{route('account.destroy', Auth::user()->id)}}" method="POST">
                @csrf
                @method('DELETE')
                &nbsp;&nbsp;&nbsp;<button class="btn btn-primary" type="sumbit"> Delete</button></form>
            @else
                <form method="POST" enctype="multipart/form-data" action="{{ route('account.store') }}">
                    @csrf
                    <input type="file" name="avatar" />
                    <input type="submit" value=" Save " />
                </form>
            @endif
                   
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        </div>
    </div>
</div>
@endsection