<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head >
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Dashboard') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <!-- Fonts -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="resources/views/layouts/style.css">

    <!-- Styles -->
</head>
<body>

    <div id="app" >
       
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm" >
            <div class="container">
                <a class="navbar-brand" href="{{ url('/dash') }}">
                    <font size="4" color = "#1C713E"> {{ config('Dashboard', 'Dashboard') }} </font>
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent" >
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <li class="dropdown"><a class="dropdown-toggle"  data-toggle="dropdown" href="#"> <font size="4" color = "#1C713E">Parks</font>  </a>
                                <ul class="dropdown-menu">
                                    <li><a href="/parks"><font size="4" color = "#1C713E">&nbsp;&nbsp;&nbsp;Index</font></a></li>
                                    @hasanyrole('Admin'|'Council')
                                    <li><a href="/parks/create"><font size="4" color = "#1C713E">&nbsp;&nbsp;&nbsp;Create</font></a></li>
                                    @endhasanyrole
                                </ul>
                            </li>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            @hasanyrole('Admin|Council')
                                <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><font size="4" color = "#1C713E">Records</font></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="records"><font size="4" color = "#1C713E">&nbsp;&nbsp;&nbsp;Index</font></a></li>
                                    <li><a href="records/create"><font size="4" color = "#1C713E">&nbsp;&nbsp;&nbsp;Create</font></a></li>
                                    </ul>
                                </li>
                            @endhasanyrole
                          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                          @role('Admin')
                            <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><font size="4" color = "#1C713E">Users</font></a>
                                <ul class="dropdown-menu">
                                    <li><a href="/users"><font size="4" color = "#1C713E">&nbsp;&nbsp;&nbsp;Index</font></a></li>
                                    <li><a href="/users/create"><font size="4" color = "#1C713E">&nbsp;&nbsp;&nbsp;Create</font></a></li>
                                </ul>
                            </li>
                        @endrole
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                          @role('Admin')
                            <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#"><font size="4" color = "#1C713E">Organisations</font></a>
                                <ul class="dropdown-menu">
                                    <li><a href="/organisations"><font size="4" color = "#1C713E">&nbsp;&nbsp;&nbsp;Index</font></a></li>
                                    <li><a href="/organisations/create"><font size="4" color = "#1C713E">&nbsp;&nbsp;&nbsp;Create</font></a></li>
                                </ul>
                            </li>
                          @endrole
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                               <a class="nav-link" href="{{ route('login') }}">  <font size="4" color = "#1C713E"> {{ __('Login') }}</font></a>
                            </li>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}"> <font size="4" color = "#1C713E"> {{ __('Register') }}</font></a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    @if(Auth::user()->getMedia('avatars')->first() != null)
                                        <img src="{{ Auth::user()->getFirstMediaUrl('avatars', 'thumb') }}">
                                    @endif
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item"  href="/account">{{ __('Account') }}</a>
                                    
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
