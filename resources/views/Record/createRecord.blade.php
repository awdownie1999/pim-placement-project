@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Add Record</h1>
        <div>
        @if ($errors->any())
            <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
            </div><br/>
        @endif
        @hasanyrole('Admin|Council')
            <form method="post" enctype="multipart/form-data" action="{{ route('records.store') }}">
                @csrf
                <br/>
                <br/>
                <div>
                    <label for="season">Season</label>
                    <select name="season_id">
                        @foreach ($seasons as $season )
                            <option value="{{$season->id}}">{{ $season->season}}</option>
                        @endforeach
                    </select>
                </div>
                <br/>
                @if($redirected == false)
                    <div>
                        <label for="partition">Partition</label>
                        <select name="partition_id">
                            @foreach ($partitions as $partition )
                                <option value="{{$partition->id}}">{{ $partition->name}}</option>
                            @endforeach
                        </select>
                    </div>
                @else
                    <label for="partition">Park Partition</label>
                    <label for="partition">{{$partitions->name}}</label>
                @endif
                <br/>
                <div class="form-group">
                    <label for="description">Description:</label>
                    <input type="text" class="form-control" name="description" />
                </div>
                <br/><br/>
                <input type="file" name="file1" />
                <br/><br/>
                <input type="file" name="file2" />
                <br/><br/>
                <input type="file" name="file3" />
                <br/><br/>
                <button type="submit" class="btn btn-secondary">Add Record</button>
            </form>
        @endhasanyrole
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        </div>
    </div>
</div>
@endsection