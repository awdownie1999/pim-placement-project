@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Records</h1>
        <div>
        @if ($errors->any())
            <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
            </div><br/>
        @endif
        @hasanyrole('Admin|Council')
            <table class="table table-striped"> 
                <thead>
                    <tr>
                        <th>Organisation</th>
                        <th>User</th>
                        <th>Park</th>
                        <th>Partition</th>
                        <th>Report Year</th>
                        <th>Season</th>
                        <th>Description</th>
                    </tr>
                </thead>
                @role('Admin')
                    <form method="POST" action="{{route('records.index')}}">
                        <select name="organisations" id="organisation">
                            @foreach ($organisations as $organisation)
                                <option value="{{$organisation->id}}">{{$organisation->name}}</option>
                            @endforeach
                        </select>
                    </form>
                @endrole
                <div><a style="margin: 19px;" enctype="multipart/form-data" href ="{{route('records.create') }}" class="btn btn-primary">New Report</a>
                    <tbody>
                        @foreach ($records as $record)
                            <tr>
                                <td>{{ $record->partition->park->organisation->name }} </td>
                                @if($record->partition->user != null)
                                    <td>{{ $record->partition->user->name }} </td>
                                @else
                                    <td>No User Assigned</td>
                                @endif
                                <td>{{ $record->partition->park->name }} </td>
                                <td><a href ="{{route('partitions.show', $record->partition->id) }}"> {{ $record->partition->name }} </a></td> 
                                <td>{{ $record->report_year }} </td>
                                <td>{{$record->season->season}}  </td>
                                <td>{{ $record->description }} </td>
                                <td><a href="{{route('records.edit', $record->id)}}" class="btn btn-primary">Edit</a></td>
                                <td><form action="{{route('records.destroy', $record->id)}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger" type="submit"> Delete</button></form></td>       
                                @if($record->getMedia('report_images') != null)
                                    @foreach ($record->getMedia('report_images') as $image)
                                        <td> &nbsp;&nbsp;&nbsp;<a href="{{ $image->getUrl()}}"> <img src="{{ $image->getUrl('small') }}"> </a>  </td>
                                    @endforeach
                                @endif       
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                @endhasanyrole
    
                <div class="row">
                    <div class="col-12 d-flex justify-content-center pt-4">
                        {{$records->links()}}
                    </div>
                </div>
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        </div>
    </div>
</div>
@endsection