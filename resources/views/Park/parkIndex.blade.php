@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
        <h1 class="display-3">Parks</h1>
        <div>
        @if ($errors->any())
            <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
            </div><br/>
        @endif
        <div>   
            @role('Admin')
                <form class="form-inline" action="/search" method="POST" role="search" >
                    {{ csrf_field() }}
                    <div class="input-group">
                        <input type="text" name="q" width="500px"
                            placeholder="Search Parks by Name"> <span class="input-group-btn">&nbsp;&nbsp;&nbsp;
                            <button type="submit" class="btn btn-primary">Search</button>
                        </span>
                    </div>
                </form>
            @endrole
           
            @hasanyrole('Admin|Council')
                <a style="margin:19px;" href="{{route('parks.create')}}" class="btn btn-primary">Create Park</a>
                <a style="margin: 19px;" href ="{{ route('partitions.create') }}" class="btn btn-primary">Create Partition</a>
                <a style="margin: 19px;" href="{{ route('parks.index', ['filter'=> 'all']) }}" class="btn btn-primary">All Parks</a>
                <a style="margin: 19px;" href="{{ route('parks.index', ['filter'=> 'assigned']) }}" class="btn btn-primary">Assigned parks</a>
            @endhasanyrole
            <a href="{{route('partitions.index')}}" class="btn btn-primary">Show Partitions</a>

        </div>
        
            <table>
                <thead>
                    <tr>
                        <th>Park Name</th>
                        <th>Organisation</th>
                    </tr>
                </thead> 
               
                <tbody>
                    @if($empty != null)
                        @foreach ($parks as $park)
                            <tr>
                                <td>{{ $park->name }}</td>
                                <td>&nbsp;&nbsp;&nbsp;{{ $park->organisation->name }}</td>
                                @hasanyrole('Admin|Council')
                                    <td>&nbsp;&nbsp;&nbsp;<a href="{{route('parks.edit', $park->id)}}" class="btn btn-primary">Edit</a></td>
                                    <td><form action="{{route('parks.destroy', $park->id)}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    &nbsp;&nbsp;&nbsp;<button class="btn btn-primary" type="sumbit"> Delete</button></form></td>
                                    @if($park->getMedia('park_images') != null)
                                        @foreach ($park->getMedia('park_images') as $image)
                                            <td> &nbsp;&nbsp;&nbsp;<a href="{{ $image->getUrl()}}"> <img src="{{ $image->getUrl('small') }}"> </a>  </td>
                                        @endforeach
                                    @endif
                                @endhasanyrole
                            </tr> 
                        @endforeach
                    @else
                            <h3>No parks matching criteria</h3>
                    @endif
                </tbody>
            </table>
                <div class="row">
                    <div class="col-12 d-flex justify-content-center pt-4">
                        {{$parks->links()}}
                    </div>
                </div>
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        </div>
    </div>
</div>
@endsection