@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Edit park</h1>
        <div>
        @if ($errors->any())
            <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
            </div><br/>
        @endif
        @hasanyrole('Admin|Council')
            <form method="post" enctype="multipart/form-data" action="{{ route('parks.update', $park->id)}}">
                @method('PATCH')
                @csrf
                <div class="form-group">
                    <label for="name">name:</label>
                    <input type="text" class="form-control" name="name" value="{{ $park->name }}">
                </div>
                <div class="form-group">
                    <label for="organisation">Organisation:</label>
                    <select name="organisation_id">
                        @foreach ($organisations as $organisation)
                            <option value="{{$organisation->id}}">{{ $organisation->name }}</option>
                        @endforeach
                    </select>
                </div>
                    <input type="file" name="file1" />
                <button type="submit" class="btn btn-secondary">Update</button>
            </form>
        @endhasanyrole
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        </div>
    </div>
</div>
@endsection