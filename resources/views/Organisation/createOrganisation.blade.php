@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Create Organisation</h1>
        <div>
        @if ($errors->any())
            <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
            </div><br/>
        @endif
        @role('Admin')
            <form method="POST" action="{{route('organisations.store')}}">
                @csrf
                <div class="form-group">
                    <label for="name">Name:</label>
                    <input type="name" class="form-control" name="name" />
                </div>
                <button type="submit" class="btn btn-primary">Add Organisation</button>
            </form>
        @endrole
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        </div>
    </div>
</div>
@endsection