@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Users</h1>
        <div>
        @if ($errors->any())
            <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
            </div><br/>
        @endif
        <div><a style="margin: 19px;" href ="{{ route('users.create') }}" class="btn btn-primary">New User</a>
            <a style="margin: 19px;" href="{{ route('users.index', ['filter'=> 'all']) }}" class="btn btn-primary">All Users</a>
            <a style="margin: 19px;" href="{{ route('users.index', ['filter'=> 'reg']) }}" class="btn btn-primary">Assigned Users </a>
            <a style="margin: 19px;" href="{{ route('users.index', ['filter'=> 'noReg']) }}" class="btn btn-primary">Unassigned Users </a></div>
        @role('Admin')
            @if($users != null)
                <table class="table table-striped"> 
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Contact Number</th>
                            <th>Address</th>
                            <th>Organisation</th>
                            <th>Park</th>
                            <th>Partition</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($users as $user)
                            <tr>
                                <td>{{ $user->name }} </td>
                                <td>{{ $user->email }} </td>
                                <td>{{ $user->contact_num }} </td>
                                <td>{{ $user->address }} </td>
                                
                                @if($user->organisation != null)
                                    <td>{{$user->organisation->name}} </td>
                                @else
                                    <td>No Organisation</td>
                                @endif

                                @if($user->partition != null && $user->organisation !=null)
                                    <td>{{ $user->partition->park->name }}  </td> 
                                    <td>{{ $user->partition->name }}  </td> 
                                @else
                                    <td>N/A</td>
                                    <td>N/A</td>
                                @endif
                                
                                <td><a href="{{route('users.edit', $user->id)}}" class="btn btn-primary">Edit</a></td>
                                <td><form action="{{route('users.destroy', $user->id)}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger" type="submit"> Delete</button></form></td>                           
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @endif
        @endrole
        @if($users != null)
            <div class="row">
                <div class="col-12 d-flex justify-content-center pt-4">
                   {{$users->links() }}
                </div>
            </div>
        @else
            <h3>No users matching criteria</h3>
        @endif
            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif
        </div>
    </div>
</div>
@endsection