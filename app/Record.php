<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;


class Record extends Model implements HasMedia
{
    use HasMediaTrait;
    protected $fillable = [
        'report_year', 'season_id', 'partition_id', 'description'
    ];

    public function partition(){
        return $this->belongsTo('App\Partition');
    }

    public function season(){
        return $this->belongsTo('App\Season');
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('small')
            ->width(150)
            ->height(150);
    }
}
