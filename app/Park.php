<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class Park extends Model implements HasMedia
{
    use HasMediaTrait;
    
    protected $fillable = [
        'name', 'organisation_id',
    ];

    public function organisation(){
        return $this->belongsTo('App\Organisation');
    }

    public function partition(){
        return $this->hasMany('App\Partition');
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
            ->width(50)
            ->height(50);
           
        $this->addMediaConversion('small')
            ->width(150)
            ->height(150);
    }
}
