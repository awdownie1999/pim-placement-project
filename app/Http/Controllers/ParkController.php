<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Park;
use App\Organisation;
use App\Partition;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;


class parkController extends Controller
{

    public function paginate($items, $perPage = 5, $page = null, $options = [])
    {
    $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
    $items = $items instanceof Collection ? $items : Collection::make($items);
    return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = $request->get('filter');
        $partitions = Partition::all();
        $unassignedparks = array();
        $fullyAssignedparks = array();
        $empty = 'Not Empty';

        if($request->has('q')){
            Log::info('Query Check : ' . $request->get('q'));
            $q = $request->get( 'q' );
            Log::info('Query Check : ' . $q);
            $parks = Park::where('name', 'LIKE', '%' . $q . '%')->get();
            Log::info('Parks Array : ' . $parks);
            if (count ( $parks ) > 0) {
                $parks = $this->paginate($parks);
            }else{
                $parks = Park::paginate(5);
                $empty = null;
            }
        }else {
            if($filter == null || $filter == 'all'){
                Log::info('data');
                if(Auth::user()->organisation_id != null){
                    $parks = Park::where('organisation_id', Auth::user()->organisation_id)->orderBy('organisation_id', 'DESC')->paginate(10);
                }else{
                    $parks = Park::paginate(5);
                }

            }else if($filter = 'assigned'){
                $parks = Park::all();
                foreach($partitions as $partition){
                    Log::info('Data' );
                    if($partition->user == null){
                        Log::info('Check UnAssigned Array Element' . $partition);
                        $parksWithUnassignedPartitions = $partition->park_id;
                        $unassignedpark = Park::where('id', $parksWithUnassignedPartitions)->first();
                        array_push($unassignedparks, $unassignedpark);
                    }//if
                }// foreach
                foreach($parks as $park){
                    $bool = false;
                    foreach($unassignedparks as $unassigned){
                        if($park->id == $unassigned->id){
                            $bool = true;
                            Log::info('Check');
                            break;
                        }//if
                    }//foreach
                    if(! $bool){
                        if(Auth::user()->organisation_id !=null){
                            $park = Park::where('organisation_id', Auth::user()->organisation_id)->first();
                            array_push($fullyAssignedparks, $park);
                        }else{
                            array_push($fullyAssignedparks, $park);
                        }//if
                    }//if
                }//foreach

                if(count($fullyAssignedparks) > 0){
                    $parks = $this->paginate($fullyAssignedparks);
                }else{
                    $parks = Park::all();
                    $empty = null;
                }
            }
        }//if
        return view('Park.parkIndex', compact('parks', 'empty', 'parks'));    
    }//class

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::user()->organisation_id != null){
            $parks = Park::all();
            $organisations = Organisation::where('id', Auth::user()->organisation_id)->get();
        }else{
            $parks = Park::all();
            $organisations = Organisation::all();
        }
        return view('Park.createPark', compact('parks', 'organisations'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'organisation_id' => 'required',
        ]);

        $park = park::firstOrNew([
            'name' => $request->name,
            'organisation_id' => $request->organisation_id,
        ]);
        $park->save();


        if($request->hasFile('file1') && $request->file('file1')->isValid()){
            $park->addMediaFromRequest('file1')->toMediaCollection('park_images');
        }

        if($request->hasFile('file2') && $request->file('file2')->isValid()){
            $park->addMediaFromRequest('file2')->toMediaCollection('park_images');
        }

        if($request->hasFile('file3') && $request->file('file3')->isValid()){
            $park->addMediaFromRequest('file3')->toMediaCollection('park_images');
        }


        return redirect('/parks')->with('Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $park = Park::find($id);
        $organisations = Organisation::all();
 
        return view('Park.editPark', compact('park', 'organisations'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'organisation_id' => 'required|numeric',
        ]);

        $park = Park::find($id);
        $park->name = $request->get('name');
        $park->organisation_id = $request->get('organisation_id');
        $park->save();
        $parkMedia = $park->getMedia('park_images');

        if($request->hasFile('file1') && $request->file('file1')->isValid()){
            $park->addMediaFromRequest('file1')->toMediaCollection('park_images');
        }

        return redirect('/parks')->with('Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $park = Park::find($id);
        $park->delete();

        return redirect('/parks')->with('park deleted!');
    }

    public function deleteImage($id, Request $request){


        return redirect('/parks')->with('deleted!');
    }
}