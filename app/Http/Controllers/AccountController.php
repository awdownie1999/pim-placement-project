<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Log;
use App\Organisation;
use Illuminate\Support\Facades\Auth;
use App\User;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        return view('Account.account', compact('user'));
    }

    public function store(Request $request)
    {
        //Populate model
        $user =  User::firstOrNew([
            'name' => Auth::user()->name,
            'email' => Auth::user()->email,
            'password' => Auth::user()->password,
            'address' => Auth::user()->address,
            'contact_num' => Auth::user()->contact_num,
            'organisation_id' => Auth::user()->organisation_id,
        ]);

        //Store Image
        Log::info($request->get('avatar'));
        if($request->hasFile('avatar') && $request->file('avatar')->isValid()){
            $user->addMediaFromRequest('avatar')->toMediaCollection('avatars');
        }

        return redirect("/account")->with('image stored');
    }

/**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $user = Auth::user();
       return view('Account.editAccount', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|max:255',
            'address' => 'required|string|min:10',
            'contact_num' => 'required|numeric|min:11',
        ]);

        $user = User::find($id);
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->address = $request->get('address');
        $user->contact_num = $request->get('contact_num');
        $user->save();
        return redirect('/account')->with('account updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $userMedia = User::find($id)->getMedia('avatars')->first();
        Log::info('User Media Check : ' . $userMedia);
        $userMedia->delete();
        return redirect('/account')->with('deleted');
    }
}