<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Partition;
use App\Record;
use App\Season;
use App\Park;
use App\Organisation;
use Illuminate\Support\Facades\Auth;
use Log;

class RecordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $organisations = Organisation::all();
        
        if(Auth::user()->organisation_id == null){
            $records = Record::orderBy('id', 'DESC')->paginate(10);
        }else{
            $records = Record::where('partition_id', Auth::user()->organisation_id)->paginate(10);
        }
        
        return view('Record.recordIndex', compact('records', 'organisations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $records = Record::all();
        $seasons = Season::all();
        $redirected = false;

        Log::info('Check : ' . $request->get('partitionID'));

        if($request->has('partitionID')){
            $id = $request->get('partitionID');
            $partitions = Partition::find($id);
            $redirected = true;
        }else{
            if (Auth::user()->organisation_id != null){
                $park = Park::where('organisation_id', Auth::user()->organisation_id )->first(); 
                Log::info('Park Check : ' . $park);
                $partitions = Partition::where('park_id', $park->id)->get();
            }else{
                $partitions = Partition::all();
            }
        }
        $user = Auth::user();
        
        return view('Record.createRecord', compact('records', 'seasons', 'partitions', 'redirected'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Log::info('description check : ' . $request->get('description'));

        $request->validate([
            'description' => 'required|string',
            'season_id' => 'required',
            'partition_id' => 'required',
        ]);

            Log::info('Partition Check : ' . $request->get('partition_id'));

        $record = Record::firstOrNew([
            'report_year' => date("Y"),
            'description' => $request->get('description'),
            'season_id' => $request['season_id'],
            'partition_id' => $request->get('partition_id')
        ]);
        $record->save();

        if($request->hasFile('file1') && $request->file('file1')->isValid()){
            $record->addMediaFromRequest('file1')->toMediaCollection('report_images');
        }

        if($request->hasFile('file2') && $request->file('file2')->isValid()){
            $record->addMediaFromRequest('file2')->toMediaCollection('report_images');
        }

        if($request->hasFile('file3') && $request->file('file3')->isValid()){
            $record->addMediaFromRequest('file3')->toMediaCollection('report_images');
        }
        $record->save();

        return redirect('/records');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $record = Record::find($id);
        return view('Record.editRecord', compact('record'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'description' => 'required|string',
        ]);

        $record = Record::find($id);
        $record->description = $request->get('description');
        $record->save();

        if($request->hasFile('file1') && $request->file('file1')->isValid()){
            $record->addMediaFromRequest('file1')->toMediaCollection('report_images');
        }
        return redirect('/records')->with('updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $record = Record::find($id);
        $record->delete();
        return redirect('/records')->with('Deleted!');
    }
}
