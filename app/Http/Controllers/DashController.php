<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Partition;
use App\Park;
use App\Organisation;
use App\Record;

class DashController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $users = User::all();
        $totalUsers = count($users);
        $organisations = Organisation::all();
        $totalOrg = count($organisations);
        $parks = Park::all();
        $totalParks = count($parks);
        $partitions = Partition::all();
        $totalPartitions = count($partitions);
        $records = Record::all();
        $totalRecords = count($records);

        return view('Dash.dash', compact('totalUsers', 'totalOrg', 'totalParks', 'totalPartitions', 'totalRecords'));

    }

    
}