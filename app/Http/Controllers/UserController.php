<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Organisation;
use Log;
use Hash;
use App\Partition;
use Illuminate\Support\Facades\Auth;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class UserController extends Controller
{

    public function paginate($items, $perPage = 5, $page = null, $options = [])
    {
    $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
    $items = $items instanceof Collection ? $items : Collection::make($items);
    return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = $request->get('filter');
        $partitions = Partition::all();
        $assignedUsers = array();
        $unassignedUsers = array();
        $users = User::all();

        foreach($partitions as $partition){
            Log::info('Partition Id Check : ' . $partition);
            Log::info('user Id Check : ' . $partition->id);
            if($partition->user_id !=null){
                array_push($assignedUsers, $partition->user); 
            }
        }
        foreach($users as $user){
            if(Auth::user()->roles->first()->name == 'Citizen'){
                if(! in_array($user, $assignedUsers)){
                    array_push($unassignedUsers, $user);
                }
            }
        }//for

        if ($filter == 'all'|| $filter == null){
            $users = User::orderBy('id', 'DESC')->paginate(10);
        }else if($filter == 'reg'){
            if($assignedUsers != null){
                $users = $this->paginate($assignedUsers);
            }else{
                $users = null;
            }
        }else if($filter == 'noReg'){
            if($unassignedUsers != null){
                $users = $this->paginate($unassignedUsers);
            }else{
                $users = null;
            }
        }//else
        return view('User.userIndex', compact('users', 'filter',));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       $users = User::all();
       $organisations = Organisation::all();

       return view('User.createUser', compact('users', 'organisations'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|max:255',
            'password' => 'required|string|min:8',
            'address' => 'required|string|min:5',
            'contact_num' => 'required|numeric|min:11',
            'organisation_id' => 'nullable|numeric',
        ]);
        $user = User::firstOrNew([
            'email' => $request['email'],
        ]);
        Log::info('Address Check : ' . $request->get('address'));

        $user->fill([
            'name' => $request->name,
            'address' => $request->address,
            'contact_num' => $request->contact_num,
            'organisation_id' => $request->organisation_id,
            'password' => Hash::make($request->password),
            ]);
            $user->save();

        return redirect('/users')->with('User added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $organisations = Organisation::all();
 
        return view('User.editUser', compact('user', 'organisations'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|max:255',
            'password' => 'required|string|min:8',
            'address' => 'required|string|min:10',
            'contact_num' => 'required|numeric|min:11',
            'organisation_id' => 'nullable|numeric',
        ]);

        $user = User::find($id);
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->address = $request->get('address');
        $user->contact_num = $request->get('contact_num');
        $user->organisation_id = $request->get('organisation_id');
        $user->save();

        //Role

        $user->assignRole($request->input('roles')); 
        return redirect('/users')->with('updated user');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();

        return redirect('/users')->with('User deleted!');
    }
}
