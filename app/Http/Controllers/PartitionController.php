<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Partition;
use App\Park;
use App\User;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;

class PartitionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filter = $request->get('filter');
        $partitions = Partition::all();

        
        if($filter == 'all'|| $filter == null){
            Log::info('Check : ');

            if(Auth::user()->roles->first()->name == 'Admin'){
                Log::info('Check : ');
                $partitions = Partition::all();

            }else if(Auth::user()->roles->first()->name == 'Council'){


                $parks= Park::where('organisation_id', Auth::user()->organisation_id)->get();
                $councilArray = array();
                foreach($parks as $park){
                    Log::info('Park Check : ' . $park);
                    $councilPartitions = Partition::where('park_id', $park->id)->get();
                    Log::info('councilPartitions : ' . $councilPartitions);

                    foreach($councilPartitions as $partition){
                        array_push($councilArray, $partition);
                    }
                }
                Log::info(print_r($councilArray, true));

                if(count($councilArray) > 0){
                    $partitions = $councilArray;
                }else{
                    $partitions = null;
                }

            }else if(Auth::user()->roles->first()->name == 'Citizen'){
                $citizenArray = array();
                foreach($partitions as $partition){
                    if($partition->user_id == Auth::user()->id){
                        array_push($citizenArray, $partition);
                    }
                }
                if(count($citizenArray) > 0){
                    $partitions = $citizenArray;
                }else{
                    $partitions = null;
                }
                Log::info(print_r($partitions, true));
            }
        }else if($filter == 'due'){
            if(Auth::user()->organisation_id != null){
                $parks= Park::where('organisation_id', Auth::user()->organisation_id)->get();
            }else{
                $parks= Park::all();
            }
            $duePartitionsArray = array();
                foreach($parks as $park){
                    $duepartitions = Partition::where('park_id', $park->id)->where('days_council_service_end', '<=', 30)->get();
                    foreach($duepartitions as $partition){
                        array_push($duePartitionsArray, $partition );
                    }//for
                }//for

            if(count($duePartitionsArray) > 0){
                $partitions = $duePartitionsArray;
            }else{
                $partitions = null;
            }

        }else if($filter == 'unassigned'){
            $unassignedPartitions = array();
            if(Auth::user()->roles->first()->name == 'Admin'){
                foreach($partitions as $partition){
                    if($partition->user_id == null){
                        array_push($unassignedPartitions, $partition);
                    }
                }// foreach
            }else {
                $parks= Park::where('organisation_id', Auth::user()->organisation_id)->get();
                foreach($parks as $park){
                    $unassigned = Partition::where('park_id', $park->id)->get();
                    foreach($unassigned as $partition){
                        if($partition->user_id == null){
                            array_push($unassignedPartitions, $partition );
                        }
                    }//for
                }//for
            }
            $partitions = $unassignedPartitions;
        }
        return view('Partition.partitionIndex', compact('partitions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $partition = Partition::all();
        if(Auth::user()->organisation_id != null){
        $parks = Park::where('organisation_id', Auth::user()->organisation_id)->get();
        }else{
            $parks = Park::all();
        }
        
        return view('Partition.createPartition', compact('partition', 'parks'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            ]);

            $partition = Partition::firstOrNew([
                'name' => $request->name,
                'park_id' => $request->park_id,
            ]);
            $partition->save();

            if($request->hasFile('file1') && $request->file('file1')->isValid()){
                $partition->addMediaFromRequest('file1')->toMediaCollection('partition_images');
            }
    
            if($request->hasFile('file2') && $request->file('file2')->isValid()){
                $partition->addMediaFromRequest('file2')->toMediaCollection('partition_images');
            }
    
            if($request->hasFile('file3') && $request->file('file3')->isValid()){
                $partition->addMediaFromRequest('file3')->toMediaCollection('partition_images');
            }
            $partition->save();
            return redirect('/parks')->with('Added');
        }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $partition = Partition::find($id);
        return view('Partition.showPartition', compact('partition'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $partition = Partition::find($id);
        if(Auth::user()->organisation_id == null){
            //Give Citizens not Assigned
            $users = User::whereHas("roles", function(Builder $q) { $q->where("name", "Citizen"); })->get();

        }else{
            //give citizens from org ID
            $users = User::whereHas("roles", function(Builder $q) {
             $q->where("name", "Citizen")->where('organisation_id', Auth::user()->organisation_id); })->get();
        }
        return view('Partition.editPartition', compact('users', 'partition'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $partition = Partition::find($id);
        if($request->has('user_id')){
            Log::info('Check');
            $partitions = Partition::all();

            foreach($partitions as $p){
                if($p->user_id == $request->get('user_id')){
                    $p->user_id = null;
                    $p->save();
                }
            }
            $partition->user_id =  $request->get('user_id');
            $partition->days_council_service_end =  $request->get('days');
        }else{
            Log::info('Data');
            if($request->hasFile('file1') && $request->file('file1')->isValid()){
                $partition->addMediaFromRequest('file1')->toMediaCollection('partition_images');
            }
    
            if($request->hasFile('file2') && $request->file('file2')->isValid()){
                $partition->addMediaFromRequest('file2')->toMediaCollection('partition_images');
            }
    
            if($request->hasFile('file3') && $request->file('file3')->isValid()){
                $partition->addMediaFromRequest('file3')->toMediaCollection('partition_images');
            }
        }
        $partition->save();
        return redirect('/partitions')->with('Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $partition = Partition::find($id);
        $partition->delete();

        return redirect('/parks')->with('Deleted!');
    }

    public function addImages($id, Request $request)
    {
        Log::info("Entered here");
        $partition = Partition::find($id);
        if($request->hasFile('file1') && $request->file('file1')->isValid()){
            $partition->addMediaFromRequest('file1')->toMediaCollection('partition_images');
        }

        if($request->hasFile('file2') && $request->file('file2')->isValid()){
            $partition->addMediaFromRequest('file2')->toMediaCollection('partition_images');
        }

        if($request->hasFile('file3') && $request->file('file3')->isValid()){
            $partition->addMediaFromRequest('file3')->toMediaCollection('partition_images');
        }
        $partition->save();
        return redirect('/parks')->with('Added');
    }


/*
 
*/

}
