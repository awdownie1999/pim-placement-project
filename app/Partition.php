<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;

class Partition extends Model implements HasMedia
{
    protected $fillable = [
        'name', 'park_id', 'user_id', 'days_council_service_end',
    ];

    use HasMediaTrait;
    
    public function park(){
        return $this->belongsTo('App\Park');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function record() {
        return $this->hasOne('App\Record');
    }

    public function registerMediaConversions(Media $media = null)
    {
           
        $this->addMediaConversion('small')
            ->width(150)
            ->height(150);
    }
}
