<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organisation extends Model
{
    protected $fillable = [
        'name',
    ];

    public function park(){
       return $this->hasMany('App\Park');
    }

    public function user(){
        return $this->hasMany(('App\User'));
    }
}
